<?php

namespace App\EventSubscriber;

use App\Repository\TaskRepository;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Event\ControllerEvent;
use Twig\Environment;

class TwigEventSubscriber implements EventSubscriberInterface
{
    private $taskRepository;
    private $twig;

    public function __construct(TaskRepository $taskRepository, Environment $twig){
       $this->taskRepository = $taskRepository;
       $this->twig = $twig;
    }

    public function onKernelController(ControllerEvent $event)
    {

        if($taskId = $event->getRequest()->get('id')){
            $this->twig->addGlobal('taskId', $taskId);
        } else {
            $this->twig->addGlobal('taskId', null );
        }
    }

    public static function getSubscribedEvents()
    {
        return [
            'kernel.controller' => 'onKernelController',
        ];
    }
}
