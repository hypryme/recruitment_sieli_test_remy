<?php

namespace App\DataFixtures;

use App\Entity\Category;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class CategoryFixture extends Fixture
{
    public function load(ObjectManager $manager)
    {
        // $product = new Product();
        // $manager->persist($product);
        $labels = ['category1', 'category2', 'category3', 'category4'];

        foreach($labels as $label){
            $category = new Category();
            $category->setLabel($label);
            $manager->persist($category);
        }

        $manager->flush();
    }
}
