<?php

namespace App\DataFixtures;

use App\Entity\Task;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class TaskFixture extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $labels= ['Concevoir l\'application', 'Développer', 'Tester', 'Déployer', 'Maintenir'];

        foreach ($labels as $label) {
            $task = new Task();
            $task->setLabel($label);
            $manager->persist($task);
        }

        $manager->flush();
    }
}
