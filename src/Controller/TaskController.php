<?php

namespace App\Controller;


use App\Entity\Task;
use App\Form\TaskType;
use App\Repository\CategoryRepository;
use App\Repository\TaskRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/task")
 */
class TaskController extends AbstractController
{
    CONST FILTER_TASK = 0; // token which aims to display the back link on general list when tasks are filtered
    CONST NO_TASK = 'No task found for that category';

    /**
     * @Route("/{_locale<%app.supported_locales%>}/", name="task_index", methods={"GET", "POST"})
     * @param TaskRepository $taskRepository
     * @param CategoryRepository $categoryRepository
     * @param Request $request
     * @return Response
     */
    public function index(TaskRepository $taskRepository, CategoryRepository $categoryRepository, Request $request): Response
    {
        $messages = [];
        if ("POST" === $request->getMethod()) {
            $categoryId = $request->request->get('category');
            $category = $categoryRepository->find($categoryId);

            if (!empty($category)) {
                $tasks = $category->getTasks();

                if (empty($tasks)) {
                    $this->addFlash(
                        'warning',
                        self::NO_TASK
                    );
                }

                return $this->render('task/index.html.twig', [
                    'tasks' => $tasks,
                    'categories' => $categoryRepository->findAll(),
                    'messages' => $messages,
                    'filterTask' => self::FILTER_TASK
                ]);
            }
        }

        return $this->render('task/index.html.twig', [
            'tasks' => $taskRepository->findAll(),
            'categories' => $categoryRepository->findAll(),
        ]);

    }

    /**
     * @Route("/{_locale<%app.supported_locales%>}/new", name="task_new", methods={"GET","POST"})
     * }
     * @param Request $request
     * @return Response
     */
    public function new(Request $request): Response
    {
        $task = new Task();

        $form = $this->createForm(TaskType::class, $task);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $manager = $this->getDoctrine()->getManager();
            $manager->persist($task);
            $manager->flush();

            return $this->redirectToRoute('task_index');
        }

        return $this->render('task/new.html.twig', [
            'task' => $task,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{_locale<%app.supported_locales%>}/{id}", name="task_show", methods={"GET"})
     * @param Task $task
     * @param Request $request
     * @return Response
     */
    public function show(Task $task, Request $request): Response
    {

        return $this->render('task/show.html.twig', [
            'task' => $task,
        ]);
    }

    /**
     * @Route("/{_locale<%app.supported_locales%>}/{id}/edit", name="task_edit", methods={"GET","POST","PUT"})
     * @param Request $request
     * @param Task $task
     * @param EntityManagerInterface $manager
     * @return Response
     */
    public function edit(Request $request, Task $task, EntityManagerInterface $manager): Response
    {

        $originalCategories = new ArrayCollection();

        foreach($task->getCategories() as $category) {
            $originalCategories->add($category);
        }

        $form = $this->createForm(TaskType::class, $task);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            foreach($originalCategories as $category) {
                if (false === $task->getCategories()->contains($category)){
                    $category->getTasks()->removeElement($category);
                }
            }
            $manager->flush();

            return $this->redirectToRoute('task_index');
        }

        return $this->render('task/edit.html.twig', [
            'task' => $task,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{_locale<%app.supported_locales%>}/{id}", name="task_delete", methods={"POST"})
     * @param Request $request
     * @param Task $task
     * @return Response
     */
    public function delete(Request $request, Task $task): Response
    {
        if ($this->isCsrfTokenValid('delete'.$task->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($task);
            $entityManager->flush();
        }

        return $this->redirectToRoute('task_index');
    }
}
