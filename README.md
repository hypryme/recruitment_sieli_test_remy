A partir du projet Symfony fourni, ajouter les fonctionnalités suivantes:
- Ajouter des “catégories” de tâches (un libellé suffit)
- Lors de la création d’une tâche, donnez la possibilité d’attacher 0, 1 ou plusieurs catégories à la tâche
- Lors de la modification d’une tâche, permettre la possibilité d’attacher et/ou détacher des catégories.
- Dans la liste des tâches, afficher les différentes catégories de la tâche
- Dans la liste des tâches, afficher la date de “deadline” si elle est remplie de façon “user friendly”
- Dans la liste des tâches, ajoutez un filtre permettant d’afficher seulement les tâches de la catégorie choisie
- Traduire les différents éléments anglais générés (liste, formulaires, détail) en français grâce au composant Translation de Symfony (on ne parle pas ici des fixtures, mais bien des élements dans le HTML tels que “show”, “edit”, “label”).